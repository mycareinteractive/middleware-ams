package org.aceso.webservice.jaxb;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Configuration {
	
	@XmlElement(name="config_id")
	public int id;
	
	public int getId() {
		return id;
	}
 	
	public void setId(int id) {
		this.id = id;
	}

	public Configuration(int id) {
		super();
		this.id = id;
	}

	public Configuration() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
