package org.aceso.webservice.jaxb;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="clinicaldata") 
public class ClinicalData {
	
	
	private int id;
	
	public ClinicalData(String mrn,String ptname, String room, String bed, String type,String code,
			String codeDescription, String value, String startDateTime,
			String endDateTime, String recvDateTime) {
		super();
		this.mrn = mrn;
		this.ptname = ptname;
		this.room = room;
		this.bed = bed;
		this.code = code;
		this.type = type;
		this.codeDescription = codeDescription;
		this.value = value;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.recvDateTime = recvDateTime;
	}

	private String ptname;

	private String mrn;

	private String room;

	private String bed;

	private String type;

	private String code;

	private String codeDescription;

	private String value;

	private String startDateTime;

	private String endDateTime;

	private String recvDateTime;
	
	
	public int getId() {
		return id;
	}
	@XmlElement(name="id")
	public void setId(int id) {
		this.id = id;
	}

	public String getPtname() {
		return ptname;
	}
	@XmlElement(name="ptname")
	public void setPtname(String ptname) {
		this.ptname = ptname;
	}
	
	public String getMrn() {
		return mrn;
	}
	@XmlElement(name="mrn")
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}


	public String getRoom() {
		return room;
	}
	@XmlElement(name="room")
	public void setRoom(String room) {
		this.room = room;
	}

	public String getBed() {
		return bed;
	}
	@XmlElement(name="bed")
	public void setBed(String bed) {
		this.bed = bed;
	}
	
	public String getType() {
		return type;
	}
	@XmlElement(name="type")
	public void setType(String type) {
		this.type = type;
	}


	public String getCode() {
		return code;
	}
	@XmlElement(name="code")
	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeDescription() {
		return codeDescription;
	}
	@XmlElement(name="codeDescription")
	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}

	public String getValue() {
		return value;
	}
	@XmlElement(name="value")
	public void setValue(String value) {
		this.value = value;
	}

	public String getStartDateTime() {
		 return startDateTime;
	}
	@XmlElement(name="startdate")
	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		 return endDateTime;
	}
	@XmlElement(name="enddate")
	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getRecvDateTime() {
		 return recvDateTime;
	}
	@XmlElement(name="recvdate")
	public void setRecvDateTime(String recvDateTime) {
		this.recvDateTime = recvDateTime;
	}

	public ClinicalData() {
		super();
		// TODO Auto-generated constructor stub
	}

}
