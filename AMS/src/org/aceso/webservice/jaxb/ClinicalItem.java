package org.aceso.webservice.jaxb;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.NONE)
public class ClinicalItem {
	
	@XmlElement(name="id")
	private int id;
	
	public ClinicalItem(int id,String ptname, String room, String bed, String code,
			String codeDescription, String value, String startDateTime,
			String endDateTime, String recvDateTime,
			String orderStatus, String orderNumber) {
		super();
		this.id=id;
		this.ptname = ptname;
		this.room = room;
		this.bed = bed;
		this.code = code;
		this.codeDescription = codeDescription;
		this.value = value;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.recvDateTime = recvDateTime;
		this.orderStatus = orderStatus;
		this.orderNumber = orderNumber;
	}
	
	@XmlElement(name="ptname")
	private String ptname;
	@XmlElement(name="room")
	private String room;
	@XmlElement(name="bed")
	private String bed;
	@XmlElement(name="code")
	private String code;
	@XmlElement(name="codedescription")
	private String codeDescription;
	@XmlElement(name="value")
	private String value;
	@XmlElement(name="startdate")
	private String startDateTime;
	@XmlElement(name="enddate")
	private String endDateTime;
	@XmlElement(name="recvdate")
	private String recvDateTime;
	@XmlElement(name="orderStatus")
	private String orderStatus;
	@XmlElement(name="orderNumber")
	private String orderNumber;
	
	public int getId() {
		return id;
	}
 	
	public void setId(int id) {
		this.id = id;
	}

	public String getPtname() {
		return ptname;
	}

	public void setPtname(String ptname) {
		this.ptname = ptname;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getBed() {
		return bed;
	}

	public void setBed(String bed) {
		this.bed = bed;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeDescription() {
		return codeDescription;
	}

	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStartDateTime() {
		 return (startDateTime != null) ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startDateTime): null;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		 return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getRecvDateTime() {
		 return recvDateTime;
	}

	public void setRecvDateTime(String recvDateTime) {
		this.recvDateTime = recvDateTime;
	}
	
	public ClinicalItem(){
		super();
	}
	

}
