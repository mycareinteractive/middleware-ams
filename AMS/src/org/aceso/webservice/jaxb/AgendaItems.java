package org.aceso.webservice.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.aceso.webservice.domain.Agenda;

@XmlType(propOrder = {"agendaDate", "agendaEntry"})
@XmlAccessorType(XmlAccessType.FIELD)
public class AgendaItems implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="date")
	public String agendaDate;
	
	@XmlElement(name="entry")
	public Agenda agendaEntry = new Agenda();
	
	public String getAgendaDate() {
		return agendaDate;
	}
	public void setAgendaDate(String agendaDate) {
		this.agendaDate = agendaDate;
	}
	public Agenda getAgendaEntry() {
		return agendaEntry;
	}
	public void setAgendaEntry(Agenda agendaEntry) {
		this.agendaEntry = agendaEntry;
	}
	
	

}
