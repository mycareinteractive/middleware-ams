package org.aceso.webservice.jaxb;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="configselections") 
@XmlType(propOrder={"room", "bed", "configList"})
public class ConfigSelections implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String room;
	private String bed;	
    private List<Configuration> configList= new ArrayList<Configuration>();
	
	public String getRoom() {
		return room;
	}
 
	@XmlElement(name="room") 
	public void setRoom(String room) {
		this.room = room;
	}
 
	public String getBed() {
		return bed;
	}
 
	@XmlElement(name="bed") 
	public void setBed(String bed) {
		this.bed = bed;
	}

	public List<Configuration> getConfigList() {
		return configList;
	}

	@XmlElementWrapper(name="configs")
	@XmlElement(name="config")
	public void setConfigList(List<Configuration> configList) {
		this.configList = configList;
	}

	public ConfigSelections(String room, String bed,
			List<Configuration> configList) {
		super();
		this.room = room;
		this.bed = bed;
		this.configList = configList;
	}

	public ConfigSelections(String room, String bed) {
		super();
		this.room = room;
		this.bed = bed;
	}

	public ConfigSelections() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

 
}