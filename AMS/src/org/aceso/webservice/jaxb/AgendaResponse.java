package org.aceso.webservice.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="response")
public class AgendaResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<AgendaItems> agendaList= new ArrayList<AgendaItems>();

	public List<AgendaItems> getAgendaList() {
		return agendaList;
	}

	@XmlElementWrapper(name="agendas")
	@XmlElement(name="agenda")
	public void setAgendaList(List<AgendaItems> agendaList) {
		this.agendaList = agendaList;
	}
	
}
