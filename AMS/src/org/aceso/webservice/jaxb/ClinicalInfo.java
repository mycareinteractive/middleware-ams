
package org.aceso.webservice.jaxb;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name="clinicalinfo") 
@XmlType(propOrder={"type", "mrn", "clinicalItems"})
public class ClinicalInfo implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	private String mrn;	
    private List<ClinicalItem> clinicalItems= new ArrayList<ClinicalItem>();
	
	public String getType() {
		return type;
	}
 
	@XmlElement(name="type") 
	public void setType(String type) {
		this.type = type;
	}
 
	public String getMrn() {
		return mrn;
	}
 
	@XmlElement(name="mrn") 
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public List<ClinicalItem> getClinicalItems() {
		return clinicalItems;
	}

	@XmlElementWrapper(name="items")
	@XmlElement(name="item")
	public void setClinicalItems(List<ClinicalItem> careBoardItems) {
		this.clinicalItems = careBoardItems;
	}

	public ClinicalInfo(String type, String mrn,
			List<ClinicalItem> careBoardItems) {
		super();
		this.type = type;
		this.mrn = mrn;
		this.clinicalItems = careBoardItems;
	}

	public ClinicalInfo(String type, String mrn) {
		super();
		this.type = type;
		this.mrn = mrn;
	}

	public ClinicalInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

 
}