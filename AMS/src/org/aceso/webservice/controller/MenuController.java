package org.aceso.webservice.controller;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;


import org.aceso.webservice.jaxb.ClinicalData;
import org.aceso.webservice.jaxb.AgendaResponse;
import org.aceso.webservice.jaxb.ClinicalInfo;
import org.aceso.webservice.jaxb.ClinicalItem;
import org.aceso.webservice.jaxb.ConfigSelections;
import org.json.JSONObject;
import org.json.XML;
import org.aceso.webservice.service.AMSService;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;




/**
 * REST service provider
 * 
 * Only GET and POST will return values
 * PUT and DELETE will not.
 */
@Controller
public class MenuController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Resource(name="amsService")
	private transient AMSService amsService;
	public static int PRETTY_PRINT_INDENT_FACTOR = 4;


   // Underneath the covers, Spring MVC delegates to a HttpMessageConverter to perform the serialization. In this case, Spring MVC invokes a MappingJacksonHttpMessageConverter built on the Jackson JSON processor. This implementation is enabled automatically when you use the mvc:annotation-driven configuration element with Jackson present in your classpath.
    @RequestMapping(value = "/getMenuJson", 
			method = RequestMethod.GET)
	public void getMenuJson(@RequestParam("portal") String portalType,@RequestParam("locale") String localeId,
							HttpServletResponse response) {
    	try {
    		log.info("Entering MenuController.getMenuJson for portal ="+portalType);
    		int locale = 0;
    		//TODO - verify if the localeId is valid or not by querying the database.
    		if(localeId != null || localeId.trim().length() != 0)
    			locale = Integer.parseInt(localeId);
    		
    		String xml = amsService.generateXML(portalType,locale);
    		/*XMLSerializer xmlSerializer = new XMLSerializer(); 
			JSON json = xmlSerializer.read( xml );  
    		PrintWriter printWriter = null;	        
	        printWriter = response.getWriter();  
			printWriter.println(json.toString(2));
			printWriter.close();*/
			
			JSONObject xmlJSONObj = XML.toJSONObject(xml);
            String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
            //log.debug(jsonPrettyPrintString);
            PrintWriter printWriter = null;	        
	        printWriter = response.getWriter(); 
	        printWriter.println(jsonPrettyPrintString);
	        printWriter.close();
	        
		} catch (Exception e) {
			log.error("Exception in MenuController.getMenuJson for portal ="+portalType,e);
		}
    	log.info("Exiting MenuController.getMenuJson for portal ="+portalType);
    }
    
    //http://localhost:8080/ams/aceso/getMenuXml?portal=garf
    @RequestMapping(value = "/getMenuXml", 
			method = RequestMethod.GET)
	public void getMenuXml(@RequestParam("portal") String portal,
						 @RequestParam(value = "locale", defaultValue = "0") String locale,
							HttpServletResponse response) {
    	try {
    		log.info("Entering MenuController.getMenuXML for portal ="+portal);
    		int localeId = 0;
    		//TODO - verify if the localeId is valid or not by querying the database.
    		if(locale != null && locale.trim().length() != 0)
    			localeId = Integer.parseInt(locale);
    		
    		String xml = amsService.generateXML(portal,localeId);
    		PrintWriter printWriter = null;	        
	        printWriter = response.getWriter();
			printWriter.println(xml);
			printWriter.close();
		} catch (Exception e) {
			log.error("Exception in MenuController.getMenuXML ",e);
		}
    	log.info("Exiting MenuController.getMenuXML for portal ="+portal);
    }
    
      
    @RequestMapping(value = "/addConfigSelections", 
			method = RequestMethod.POST)
	public void getAddConfigurationSelections(@RequestBody String configParams, HttpServletResponse response) {
		
    	log.info("Entering MenuController.getAddConfigurationSelections for configParams ="+configParams);
		JAXBContext jc;
		Map<String, String> map = new HashMap<String, String>();
		String configXml = null;
		try {
			jc = JAXBContext.newInstance(ConfigSelections.class);
			Unmarshaller u = jc.createUnmarshaller(); 
			/*if(configParams.indexOf("&") >0){
				   String split[] = configParams.split("&");
				   for(int i=0;i<split.length;i++){
					   int firstIndex = split[i].indexOf("=");
						map.put( split[i].substring(0, firstIndex),  split[i].substring(firstIndex+1));
				   }
			}else if(configParams.indexOf("=") >0){
				int firstIndex = configParams.indexOf("=");
				map.put(configParams.substring(0, firstIndex), configParams.substring(firstIndex+1));
			}else{
				configXml = configParams;
			}
			Iterator<String> paramIterator = map.keySet().iterator();            
			while (paramIterator.hasNext()) {                
				String key = paramIterator.next();               
				String value = map.get(key);  
				if(key.equalsIgnoreCase("configSels")){
					configXml = value;
				}
			} 	*/
			configXml = configParams;
			StringReader reader = new StringReader(configXml);
			ConfigSelections configSelections =(ConfigSelections) u.unmarshal( reader);
			amsService.addConfigSelections(configSelections);
		} catch (JAXBException e) {
			log.error("JAXBException in MenuController.getAddConfigurationSelections ",e);
		} catch (Exception e) {
			log.error("Exception in MenuController.getAddConfigurationSelections",e);
		} 
		log.info("Exiting MenuController.getAddConfigurationSelections for configSels ="+configXml);

    }
    
    @RequestMapping(value = "/getConfigSelections", 
			method = RequestMethod.GET)
	public void getConfigurationSelections(@RequestParam("room") String room, @RequestParam("bed") String bed, HttpServletResponse response) {
    	log.info("Entering MenuController.getConfigurationSelections for room,bed ="+room+","+bed); 
		JAXBContext jc;
		ConfigSelections configSelections = null;
		
		try {
			configSelections = amsService.getConfigSelections(room, bed);
			jc = JAXBContext.newInstance(ConfigSelections.class);
			Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        log.debug("ConfigSelections "+configSelections);
	       // marshaller.marshal(configSelections, System.out);

	        StringWriter st = new StringWriter(); 
	        marshaller.marshal(configSelections , st); 
	        String xml = st.toString();
	        PrintWriter printWriter = null; 
	        printWriter = response.getWriter();
	        printWriter.println(xml);
	        printWriter.close();


		} catch (JAXBException e) {
			log.error("JAXBException in MenuController.getConfigurationSelections ",e);
		} catch (Exception e) {
			log.error("Exception in MenuController.getConfigurationSelections",e);
		} 
		log.info("Exiting MenuController.getConfigurationSelections ");
    }
    @RequestMapping(value = "/getAgendaXml", 
			method = RequestMethod.GET)
	public void getAgendaXml(@RequestParam("portal") String portalType,@RequestParam("days") int days, @RequestParam("startdate") String dateOverride,  HttpServletResponse response) {
    	
    	JAXBContext jc;
    	AgendaResponse agendaResponse = null;
		
		try {
			agendaResponse = amsService.findAgendaResponse(days,portalType,dateOverride);
			jc = JAXBContext.newInstance(AgendaResponse.class);
			Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        StringWriter sw = new StringWriter();
	        marshaller.marshal(agendaResponse, sw);
	        System.out.println(sw.toString());
	        
	        
	        

		} catch (JAXBException e) {
			log.error("JAXBException in MenuController.getAgendaXml ",e);
		} catch (Exception e) {
			log.error("Exception in MenuController.getAgendaXml",e);
		} 
		log.info("Exiting MenuController.getAgendaXml");
		
    }
    
    
    @RequestMapping(value = "/getAgendaJson", 
			method = RequestMethod.GET)
	public void getAgendaJson(@RequestParam("portal") String portalType,@RequestParam("days") int days, @RequestParam("startdate") String dateOverride, HttpServletResponse response) {
    	
    	JAXBContext jc;
    	AgendaResponse agendaResponse = null;
		
		try {
			agendaResponse = amsService.findAgendaResponse(days,portalType,dateOverride);
			jc = JAXBContext.newInstance(AgendaResponse.class);
			Marshaller marshaller = jc.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        StringWriter sw = new StringWriter();
	        marshaller.marshal(agendaResponse, sw);
	        //System.out.println(sw.toString());
	        JSONObject xmlJSONObj = XML.toJSONObject(sw.toString());
	        String test = XML.escape(sw.toString());
            String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
            //log.debug(jsonPrettyPrintString);
            PrintWriter printWriter = null;	        
	        printWriter = response.getWriter(); 
	        printWriter.println(jsonPrettyPrintString);
	        printWriter.close();			

		} catch (JAXBException e) {
			log.error("JAXBException in MenuController.getAgendaJson ",e);
		} catch (Exception e) {
			log.error("Exception in MenuController.getAgendaJson",e);
		} 
		log.info("Exiting MenuController.getAgendaJson");
    }
    
    @RequestMapping(value = "/getClinicalData",method = RequestMethod.GET)
    public void getClinicalData(@RequestParam("type") String type,
    		@RequestParam("mrn") String mrn,
    		@RequestParam("numrec") String numrec,
    		@RequestParam("sortorder") String sortorder,
    		HttpServletResponse response) {
    	log.info("Entering ConfigController.getClinicalData for type = " + type
    			+", mrn = " + mrn +", numrec =" + numrec + ", sortorder = " + sortorder);

    	JAXBContext jc;
    	ClinicalInfo clinicalInfo = null;
    	try {
    		clinicalInfo = amsService.getClinicalData(type, mrn, numrec,sortorder);
    		jc = JAXBContext.newInstance(ClinicalInfo.class);
    		Marshaller marshaller = jc.createMarshaller();
    		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    		StringWriter sw = new StringWriter();
    		marshaller.marshal(clinicalInfo, sw);
    		String xml = sw.toString();
    		PrintWriter pw = response.getWriter();
    		pw.println(xml);
    		pw.close();
    	} catch (JAXBException e) {
    		log.error("JAXBException in ConfigController.getClinicalData ", e);
    	} catch (Exception e) {
    		log.error("Exception in ConfigController.getClinicalData", e);

    	}

    	log.info("Exiting ConfigController.getClinicalData ");

    }

    @RequestMapping(value = "/addClinicalData", 
			method = RequestMethod.POST)
	public void addClinicalData(@RequestBody String configParams, HttpServletResponse response) {
		
    	log.info("Entering ConfigController.AddClinicalData  ="+configParams);
		JAXBContext jc;
		String configXml = null;
		try {
			jc = JAXBContext.newInstance(ClinicalData.class);
			Unmarshaller u = jc.createUnmarshaller(); 
			configXml = configParams;
			StringReader reader = new StringReader(configXml);
			ClinicalData clinicalData =(ClinicalData) u.unmarshal( reader);
			amsService.addClinicalData(clinicalData);
			
		} catch (JAXBException e) {
			log.error("JAXBException in ConfigController.AddClinicalData ",e);
		} catch (Exception e) {
			log.error("Exception in ConfigController.AddClinicalData",e);
		} 
		log.info("Exiting ConfigController.AddClinicalData  ="+configXml);

    }
   
		 
}
