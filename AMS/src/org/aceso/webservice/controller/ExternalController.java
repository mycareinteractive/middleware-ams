package org.aceso.webservice.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.dmcloud.cloudkey.CloudKey;
import net.dmcloud.util.DCArray;
import net.dmcloud.util.DCObject;

import org.aceso.webservice.domain.PatronInfo;
import org.aceso.webservice.domain.PatronMenu;
import org.aceso.webservice.service.AMSService;
import org.aceso.webservice.utility.AMSUtil;
import org.aceso.webservice.utility.StringHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ExternalController {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Resource(name="amsService")
	private transient AMSService amsService;
	
	@RequestMapping(value = "/getMediaList", 
			method = RequestMethod.GET)
	public void dailyMotionMediaList(HttpServletResponse response){
		String user_id = "51315ab2947399544100ceb0";
		String api_key = "0a88b1d78791ac91dc4012e5ee11a7f054e57f03";
		
		try{    
			String server_url = "http://" + InetAddress.getLocalHost().getHostName();
			log.debug("Server url:"+server_url);
			CloudKey cloud = new CloudKey(user_id, api_key); 
			DCObject result = cloud.call("media.list",DCObject.create().push("fields", DCArray.create().push("id").push("meta.title").push("assets.jpeg_thumbnail_auto.stream_url").push("assets.mp4_h264_aac.video_width").push("assets.mp4_h264_aac.video_height").push("assets.source.download_url")));    
			DCArray list = DCArray.create((ArrayList)result.get("list"));    
			for(int i=0; i<list.size(); i++)    {        
				DCObject item = DCObject.create((Map)list.get(i));       
				log.debug("<p>Title : " + item.pull("meta.title") + "</p>");        
				log.debug("<p><img src=\"" + item.pull("assets.jpeg_thumbnail_auto.stream_url") + "\" /></p>");        
				String[] referers = {server_url};        
				String embed_url = cloud.mediaGetEmbedUrl(item.get("id").toString(), CloudKey.SECLEVEL_REFERER, "", "", "", null, referers, 0);       
				log.debug("<iframe width=\"" + item.pull("assets.mp4_h264_aac.video_width") + "\" height=\"" + item.pull("assets.mp4_h264_aac.video_height") + "\" src=\"" + embed_url  + "\"></iframe>");        
				String stream_url = cloud.mediaGetStreamUrl(item.get("id").toString(), "mp4_h264_aac", CloudKey.SECLEVEL_REFERER, "", "", "", null, referers, 0, "", false);        
				String dl_url = item.pull("assets.source.download_url");        
				log.debug("<p><a href=\"" + dl_url + "\">Download source</a></p>");        
				log.debug("<p><a href=\"" + stream_url + "\">Stream url</a></p>");    
			}
		}catch(Exception e){    
			log.debug("<p>Error : " + e.getMessage() + "</p>");
		}
	}
	
	@RequestMapping(value = "/getPatronInfo", 
			method = RequestMethod.GET)
	public void getPatronInfo(@ModelAttribute(value="patronInfo") PatronInfo patronInfo, HttpServletResponse response) {
    	String outputString="";
		try {
				System.out.println("TEST*******************************");
				if(patronInfo != null){	
					System.out.println("INSIDE *******************************");
					logPatronInfoRequestParams(patronInfo);
					if( validInput(patronInfo)){
						int count = amsService.findPatientExists(patronInfo.getName(), patronInfo.getRoom(), patronInfo.getBed());
						if(count > 0){
							String mrn = amsService.getPatientMrn(patronInfo.getName(), patronInfo.getRoom(), patronInfo.getBed());
							if(!StringHandler.isValidString(mrn)){
								//TODO ERROR 
								log.info( "  **No Valid Input** ");
							}
							String computritionRequestUrl =patronInfo.getUrl()+"?mrn="+mrn;
							URL url = new URL(computritionRequestUrl);
							HttpURLConnection conn = (HttpURLConnection) url.openConnection();
							conn.setRequestMethod("GET");
							conn.setRequestProperty("Accept", "application/xml");
					 
							if (conn.getResponseCode() != 200) {
								throw new RuntimeException("Failed : HTTP error code : "
										+ conn.getResponseCode());
							}
					 
							BufferedReader br = new BufferedReader(new InputStreamReader(
								(conn.getInputStream())));
					 
							String output;
							System.out.println("Output from Server .... \n");
							while ((output = br.readLine()) != null) {
								outputString = outputString + output;
							}
							System.out.println(outputString);
							conn.disconnect();
							PrintWriter printWriter = null;	        
						    printWriter = response.getWriter();
							printWriter.println(outputString);
							printWriter.close();
						}else{
							//TODO ERROR
							log.info( "  **No Such Patient** "); 
						}
					}else{
						log.info( "  **No Valid Input** ");
					}
				}else{
					//TODO ERROR 
					log.info( "  **No Valid Input** ");
				}
		  } catch (Exception e) {	 
			log.error("Exception",e); 
		  }
		  
    }

	private boolean validInput(PatronInfo patronInfo) {
		return AMSUtil.hasValue(patronInfo.getUrl())
				&& AMSUtil.hasValue(patronInfo.getUrl())
				&& AMSUtil.hasValue(patronInfo.getName())
				&& AMSUtil.hasValue(patronInfo.getRoom())
				&& AMSUtil.hasValue(patronInfo.getBed())
				&& AMSUtil.hasValue(patronInfo.getMac());
	}

	private void logPatronInfoRequestParams(PatronInfo patronInfo) {
		log.debug("processing with following input parameters: " 
				+ paramToNameValueString(patronInfo, PatronInfoParameters.url) + ", "
				+ paramToNameValueString(patronInfo, PatronInfoParameters.name) + ", "
				+ paramToNameValueString(patronInfo, PatronInfoParameters.room) + ", "
				+ paramToNameValueString(patronInfo, PatronInfoParameters.bed) + ", "
				+ paramToNameValueString(patronInfo, PatronInfoParameters.mac));
	}

	public enum PatronInfoParameters {
		url,
		name,
		room,
		bed,
		mac
	}
	private String paramToNameValueString(PatronInfo patronInfo, PatronInfoParameters parameterName) {		
        switch (parameterName) {
	        case url:
	        	return parameterName.toString() + "=" + patronInfo.getUrl();
	        case name:
	        	return parameterName + "=" + patronInfo.getName();
	        case room:
	        	return parameterName + "=" + patronInfo.getRoom();
	        case bed:
	        	return parameterName + "=" + patronInfo.getBed();
	        case mac:
	        	return parameterName + "=" + patronInfo.getMac();
        }
        return null;
	}
	

	@RequestMapping(value = "/getPatronMenu", 
			method = RequestMethod.GET)
	public void getPatronMenu(@ModelAttribute(value="patronMenu") PatronMenu patronMenu, HttpServletResponse response) {
    	String outputString="";
		try {
				if(patronMenu != null){	
					logPatronMenuRequestParams(patronMenu);
					if( validInput(patronMenu)){						
						String computritionRequestUrl =patronMenu.getUrl() + getUrlParams(patronMenu);
						URL url = new URL(computritionRequestUrl);
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						conn.setRequestMethod("GET");
						conn.setRequestProperty("Accept", "application/xml");
				 
						if (conn.getResponseCode() != 200) {
							throw new RuntimeException("Failed : HTTP error code : "
									+ conn.getResponseCode());
						}
				 
						BufferedReader br = new BufferedReader(new InputStreamReader(
							(conn.getInputStream())));
				 
						String output;
						System.out.println("Output from Server .... \n");
						while ((output = br.readLine()) != null) {
							outputString = outputString + output;
						}
						System.out.println(outputString);
						conn.disconnect();
						PrintWriter printWriter = null;	        
					    printWriter = response.getWriter();
						printWriter.println(outputString);
						printWriter.close();
					}else{
						//TODO ERROR 
					}
				}else{
					//TODO ERROR 
				}
		  } catch (Exception e) {	 
			log.error("Exception",e); 
		  }
		  
    }
//nutri,mrn,lock,mealid,mealdate
	private boolean validInput(PatronMenu patronMenu) {
		return AMSUtil.hasValue(patronMenu.getUrl())				
				&& AMSUtil.hasValue(patronMenu.getMrn())				
				&& AMSUtil.hasValue(patronMenu.getMealid())
				&& AMSUtil.hasValue(patronMenu.getMealdate())
				&& AMSUtil.hasValue(patronMenu.getLock())
				&& AMSUtil.hasValue(patronMenu.getNutri());
	}


	
	private void logPatronMenuRequestParams(PatronMenu patronMenu) {
		log.debug("processing with following input parameters: " 
				+ paramToNameValueString(patronMenu, PatronMenuParameters.url) + ", "
				+ paramToNameValueString(patronMenu, PatronMenuParameters.mrn) + ", "
				+ paramToNameValueString(patronMenu, PatronMenuParameters.mealid) + ", "
				+ paramToNameValueString(patronMenu, PatronMenuParameters.mealdate) + ", "
				+ paramToNameValueString(patronMenu, PatronMenuParameters.lock) + ", "
				+ paramToNameValueString(patronMenu, PatronMenuParameters.nutri));
	}
	private String getUrlParams(PatronMenu patronMenu) {
		StringBuffer urlParams = new StringBuffer();
		urlParams.append("?");
		urlParams.append(paramToNameValueString(patronMenu, PatronMenuParameters.mrn));
		urlParams.append("&");
		urlParams.append(paramToNameValueString(patronMenu, PatronMenuParameters.mealid));
		urlParams.append("&");
		urlParams.append(paramToNameValueString(patronMenu, PatronMenuParameters.mealdate));
		urlParams.append("&");
		urlParams.append(paramToNameValueString(patronMenu, PatronMenuParameters.lock));
		urlParams.append("&");
		urlParams.append(paramToNameValueString(patronMenu, PatronMenuParameters.nutri));
		return urlParams.toString();
	}
	public enum PatronMenuParameters {
		url,
		mrn,
		mealid,
		mealdate,
		lock,
		nutri
	}
	private String paramToNameValueString(PatronMenu patronMenu, PatronMenuParameters parameterName) {		
        switch (parameterName) {
	        case url:
	        	return parameterName.toString() + "=" + patronMenu.getUrl();	        
	        case mrn:
	        	return parameterName + "=" + patronMenu.getMrn();	       
	        case mealid:
	        	return parameterName + "=" + patronMenu.getMealid();
	        case mealdate:
	        	return parameterName + "=" + patronMenu.getMealdate();
	        case lock:
	        	return parameterName + "=" + patronMenu.getLock();
	        case nutri:
	        	return parameterName + "=" + patronMenu.getNutri();
        }
        return null;
	}
	
	
	
    @RequestMapping(value = "/getSelPatronMenu", 
			method = RequestMethod.GET)
	public void getSelPatronMenu(@RequestParam("url") String url, @RequestParam("xmlstring") String xmlstring, HttpServletResponse response) {
    	String outputString="";
		try {
				if(url != null){	
					log.debug("processing with following input parameters: " 
							+ url + ", "
							+ xmlstring);
					
					if( AMSUtil.hasValue(url) && AMSUtil.hasValue(xmlstring)){
						URL url1 = new URL(url);
						HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
						
						
						conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
						conn.setRequestMethod("POST");
						conn.setDoOutput(true);
						//conn.setDoInput(true);
						/*conn.setRequestMethod("GET");
						conn.setRequestProperty("Accept", "application/xml");*/
		             
		                	
		                PrintWriter pw = new PrintWriter(conn.getOutputStream());   
		               	pw.write(xmlstring);   
		                pw.close();    
		              
		                if (conn.getResponseCode() != 200) {
		              			throw new RuntimeException("Failed : HTTP error code : "
		              					+ conn.getResponseCode());
		                }
		                     
		               
				 
						BufferedReader br = new BufferedReader(new InputStreamReader(
							(conn.getInputStream())));
				 
						String output;
						System.out.println("Output from Server .... \n");
						while ((output = br.readLine()) != null) {
							outputString = outputString + output;
						}
						System.out.println(outputString);
						conn.disconnect();
						PrintWriter printWriter = null;	        
					    printWriter = response.getWriter();
						printWriter.println(outputString);
						printWriter.close();
					}else{
						//TODO ERROR 
					}
				}else{
					//TODO ERROR 
				}
		  } catch (Exception e) {	 
			log.error("Exception",e); 
		  }
		  
    }
    
    @RequestMapping(value = "/addPatient", method = RequestMethod.GET)  
    public void getAddCurrentRoomBed(HttpServletResponse response) {  
    	log.debug("Add Patient");    
    	try {
			amsService.addPatient();
			 PrintWriter printWriter = null;
		        
		        try {
					printWriter = response.getWriter();
				} catch (IOException e) {
					log.error("Exception",e);
				}
		        printWriter.println("Added Patient");
		        printWriter.close();
		} catch (Exception e) {
			log.error("Exception",e);
		}
    }
	
}
