package org.aceso.webservice.utility;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
public class DateHandler {
	
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	public static final String DATE_FORMAT_INPUT = "yyyyMMdd";

	public static String getDateIncrementByNoOfDays(String dateOverride, int noOfDaysToIncrement) {
		
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		//Calendar cal = Calendar.getInstance();
		Calendar cal =getCurrentOrOverrideDateAsCalendar(dateOverride,DATE_FORMAT_INPUT);
		cal.add(Calendar.DATE, noOfDaysToIncrement);  // number of days to add
		return sdf.format(cal.getTime());  // dt is now the new date
		
	}
	
	/*public static Date getCurrentOrOverrideDate(String currentDateOverride) throws IllegalArgumentException
	 {	 
		Calendar calendar = new GregorianCalendar(); 
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		Date overrideDate = null; 	
		if(!StringUtils.isBlank(currentDateOverride)){ 
			try { 
				overrideDate = sdf.parse(currentDateOverride);			 
			} 		
			catch (Exception e) { 
				throw new IllegalArgumentException("Error: Current Override date is not set properly"); 
			}		 
		}		 
		else { 
			overrideDate = calendar.getTime();		 
		}
		 
		return overrideDate; 
	}*/
	
	public static String getCurrentOrOverrideDateAsString(String dateOverride) {
	try { 
			DateFormat localFormat = new SimpleDateFormat(DATE_FORMAT_NOW); 
			//Date today = new Date(); 
			Date today = DateHandler.getCurrentOrOverrideDate(dateOverride,DATE_FORMAT_INPUT); 
			return localFormat.format(today); 
		} 
		catch (Exception ex) { 
			return null; 
		}	 
	} 
		 

	public static Date getCurrentOrOverrideDate(String dateOverride, String pattern) {
		Date date = null; 
		if(!StringUtils.isBlank(dateOverride)){ 
			SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US); 
			try { 
				date = sdf.parse(dateOverride);		 
			} 
			catch(Exception e) { 
				date = new Date();
			}
		}		 
		else { 
			date = new Date(); 
		}		 
		return date; 
	}

	 

	public static Calendar getCurrentOrOverrideDateAsCalendar(String dateOverride, String pattern) { 
		//Date today = new Date(); 
		Date today = DateHandler.getCurrentOrOverrideDate(dateOverride,pattern); 
		return DateHandler.getDateAsCalendar(today); 
	}
		 
	public static Calendar getDateAsCalendar(Date d) { 
		if( d == null) throw new IllegalArgumentException("Date can not be null"); 
		DateFormat localFormat = 
		new SimpleDateFormat(DATE_FORMAT_NOW); 
		/*if (localFormat.format(d).trim().equals(lowDate)) { 
		return null; 
		} 
		else {*/ 
		return DateHandler.getStringAsCalendar(localFormat.format(d)); 
		//}
		 
		}

	public static Calendar getStringAsCalendar(String s) { 
		try { 
			Calendar tempCal = Calendar.getInstance();
			 
			DateFormat localFormat = new SimpleDateFormat(DATE_FORMAT_NOW); 
			if( s == null) throw new IllegalArgumentException("String can not be null"); 
			tempCal.setTime(localFormat.parse(s));
			 
			return tempCal; 
		} 
		catch (Exception ex) { 
			return null; 
		}
	}


}
