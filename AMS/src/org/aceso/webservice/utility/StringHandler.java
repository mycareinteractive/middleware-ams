package org.aceso.webservice.utility;

public class StringHandler {
	  public StringHandler() {
	  }
	  
	  public static boolean isValidString(final String s) {
	      return ( (s != null) && (s.trim().length() > 0));
	  }
}
