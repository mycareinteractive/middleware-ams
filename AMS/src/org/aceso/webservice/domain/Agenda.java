package org.aceso.webservice.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Agenda entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "agenda")
@XmlAccessorType(XmlAccessType.FIELD)
public class Agenda implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	// Fields
	@XmlElement(name="id")
	private Integer id;
	@XmlElement(name="name")
	private String name;
	@XmlElement(name="content")
	private String content;
	@XmlElement(name="contentType")
	private String contentType;
	@XmlTransient
	private String portalType;
	@XmlTransient
	private Integer languageId;
	private Set<AgendaAttributes> agendaAttributeses = new HashSet<AgendaAttributes>(
			0);

	// Constructors

	/** default constructor */
	public Agenda() {
	}

	/** minimal constructor */
	public Agenda(String name, String content, String contentType,
			String portalType, Integer languageId) {
		this.name = name;
		this.content = content;
		this.contentType = contentType;
		this.portalType = portalType;
		this.languageId = languageId;
	}

	/** full constructor */
	public Agenda(String name, String content, String contentType,
			String portalType, Integer languageId,
			Set<AgendaAttributes> agendaAttributeses) {
		this.name = name;
		this.content = content;
		this.contentType = contentType;
		this.portalType = portalType;
		this.languageId = languageId;
		this.agendaAttributeses = agendaAttributeses;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "content", nullable = false, length = 65535)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "contentType", nullable = false, length = 20)
	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Column(name = "portalType", nullable = false, length = 20)
	public String getPortalType() {
		return this.portalType;
	}

	public void setPortalType(String portalType) {
		this.portalType = portalType;
	}

	@Column(name = "languageId", nullable = false)
	public Integer getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "agenda")
	public Set<AgendaAttributes> getAgendaAttributeses() {
		return this.agendaAttributeses;
	}

	public void setAgendaAttributeses(Set<AgendaAttributes> agendaAttributeses) {
		this.agendaAttributeses = agendaAttributeses;
	}

}