package org.aceso.webservice.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class MenuAttributes implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String value;
	private int menuId;

	private String portal;
	private String viewClass;
	private String languageId;
	private Set<MenuattributesTranslation> menuattributesTranslations = new HashSet<MenuattributesTranslation>(
			0);

	public MenuAttributes(int id, String name, String value,  String viewClass, int menuId,String portal){
		this.id = id;
		this.name = name;
		this.value = value;
		this.menuId = menuId;
		this.setViewClass(viewClass);
		this.portal = portal;
	}
	public MenuAttributes() {				
	}
	/**
	 * @return the attributeId
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param attributeId the attributeId to set
	 */
	public void setId(int attributeId) {
		this.id = attributeId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the menuId
	 */
	public int getMenuId() {
		return menuId;
	}
	/**
	 * @param menuId the menuId to set
	 */
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	
	public String getPortal() {
		return portal;
	}
	public void setPortal(String portal) {
		this.portal = portal;
	}
	public String getViewClass() {
		return viewClass;
	}
	public void setViewClass(String viewClass) {
		this.viewClass = viewClass;
	}
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public Set<MenuattributesTranslation> getMenuattributesTranslations() {
		return menuattributesTranslations;
	}
	public void setMenuattributesTranslations(
			Set<MenuattributesTranslation> menuattributesTranslations) {
		this.menuattributesTranslations = menuattributesTranslations;
	}
	

}
