
package org.aceso.webservice.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Menu implements Serializable{

	private static final long serialVersionUID = 1L;
	
    protected int id;
    protected String tag;
    protected String label;
    protected String image;
    protected int parentId;
    protected boolean attributesInd;
    protected int priority;
    private String portal;
    private String type;
	private int languageId;
	private boolean captureClicks;
	private Set<MenuTranslation> menuTranslations = new HashSet<MenuTranslation>(
			0);

    public Menu(int id, String tag, String label, String image, int parentId, boolean attributesInd, String type, int priority,boolean captureClicks,String portal,int languageId){
  	   this.id = id;
  	   this.tag = tag;
  	   this.label = label;
  	   this.image = image;
  	   this.parentId = parentId;
  	   this.attributesInd = attributesInd;
  	   this.type = type;
  	   this.languageId = languageId;
  	   this.priority = priority;
  	   this.portal = portal;
  	   this.setCaptureClicks(captureClicks);
     }     

    public Menu(){    	
    }
	/**
	 * @return the menuId
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param menuId the menuId to set
	 */
	public void setId(int menuId) {
		this.id = menuId;
	}
	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}
	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * @return the parentId
	 */
	public int getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the attributes
	 */
	public boolean isAttributesInd() {
		return attributesInd;
	}
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributesInd(boolean attributesInd) {
		this.attributesInd = attributesInd;
	}
	
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLanguageId() {
		return languageId;
	}

	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	public boolean isCaptureClicks() {
		return captureClicks;
	}

	public void setCaptureClicks(boolean captureClicks) {
		this.captureClicks = captureClicks;
	}

	public Set<MenuTranslation> getMenuTranslations() {
		return menuTranslations;
	}

	public void setMenuTranslations(Set<MenuTranslation> menuTranslations) {
		this.menuTranslations = menuTranslations;
	}




}
