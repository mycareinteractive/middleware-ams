package org.aceso.webservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * MenuTranslation entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "menu_translation")
public class MenuTranslation implements java.io.Serializable {

	// Fields

	private Integer id;
	private Menu menu;
	private Locale locale;
	private String label;

	// Constructors

	/** default constructor */
	public MenuTranslation() {
	}

	/** full constructor */
	public MenuTranslation(Integer id, Menu menu, Locale locale, String label) {
		this.id = id;
		this.menu = menu;
		this.locale = locale;
		this.label = label;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menu_id", nullable = false)
	public Menu getMenu() {
		return this.menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locale_id", nullable = false)
	public Locale getLocale() {
		return this.locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Column(name = "label", nullable = false)
	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}