
package org.aceso.webservice.domain;

import java.io.Serializable;

public class Slider implements Serializable{

	private static final long serialVersionUID = 1L;
	
	protected int id;    
    protected int menuId;
    protected String title;
    protected String columns;
    protected String column;
    protected String column1;
    protected String column2;
    protected String column3;
    protected String column4;
    protected String image;
    protected int priority;
    private String portal;

    public Slider(int id, int menuId, String title, String columns, String column, String column1, String column2, String column3, String column4, String image, int priority, String portal){
  	   this.id = id;
  	   this.menuId = menuId;
  	   this.title = title;
  	   this.columns = columns;
  	   this.column = column;
  	   this.column1 = column1;
  	   this.column2 = column2;
  	   this.column3 = column3;
  	   this.column4 = column4;
  	   this.image = image;
  	   this.priority = priority;
  	   this.portal = portal;
     }
    
    public Slider(){    	
    }
    
	/**
	 * @return the sliderId
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param sliderId the sliderId to set
	 */
	public void setId(int sliderId) {
		this.id = sliderId;
	}
	/**
	 * @return the menuId
	 */
	public int getMenuId() {
		return menuId;
	}
	/**
	 * @param menuId the menuId to set
	 */
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getColumns() {
		return columns;
	}

	/**
	 * @return the columns
	 */
	
	public void setColumns(String columns) {
		this.columns = columns;
	}
	/**
	 * @return the column1
	 */
	public String getColumn1() {
		return column1;
	}
	/**
	 * @param column1 the column1 to set
	 */
	public void setColumn1(String column1) {
		this.column1 = column1;
	}
	/**
	 * @return the column2
	 */
	public String getColumn2() {
		return column2;
	}
	/**
	 * @param column2 the column2 to set
	 */
	public void setColumn2(String column2) {
		this.column2 = column2;
	}
	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getColumn3() {
		return column3;
	}

	public void setColumn3(String column3) {
		this.column3 = column3;
	}

	public String getColumn4() {
		return column4;
	}

	public void setColumn4(String column4) {
		this.column4 = column4;
	}

}
