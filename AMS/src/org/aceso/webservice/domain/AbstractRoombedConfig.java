package org.aceso.webservice.domain;
// default package

/**
 * AbstractRoombedConfig entity provides the base persistence definition of the
 * RoombedConfig entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractRoombedConfig implements java.io.Serializable {

	// Fields

	private Integer id;
	private Configurations configurations;
	private String room;
	private String bed;

	// Constructors

	/** default constructor */
	public AbstractRoombedConfig() {
	}

	/** minimal constructor */
	public AbstractRoombedConfig(String room) {
		this.room = room;
	}

	/** full constructor */
	public AbstractRoombedConfig(Configurations configurations, String room,
			String bed) {
		this.configurations = configurations;
		this.room = room;
		this.bed = bed;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Configurations getConfigurations() {
		return this.configurations;
	}

	public void setConfigurations(Configurations configurations) {
		this.configurations = configurations;
	}

	public String getRoom() {
		return this.room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getBed() {
		return this.bed;
	}

	public void setBed(String bed) {
		this.bed = bed;
	}

}