package org.aceso.webservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * MenuattributesTranslation entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "menuattributes_translation")
public class MenuattributesTranslation implements java.io.Serializable {

	// Fields

	private Integer id;
	private Locale locale;
	private MenuAttributes menuAttributes;
	private String attrvalue;

	// Constructors

	/** default constructor */
	public MenuattributesTranslation() {
	}

	/** full constructor */
	public MenuattributesTranslation(Integer id, Locale locale,
			MenuAttributes menuAttributes, String attrvalue) {
		this.id = id;
		this.locale = locale;
		this.menuAttributes = menuAttributes;
		this.attrvalue = attrvalue;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locale_id", nullable = false)
	public Locale getLocale() {
		return this.locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menuattr_id", nullable = false)
	public MenuAttributes getMenuAttributes() {
		return this.menuAttributes;
	}

	public void setMenuAttributes(MenuAttributes menuAttributes) {
		this.menuAttributes = menuAttributes;
	}

	@Column(name = "attrvalue", nullable = false)
	public String getAttrvalue() {
		return this.attrvalue;
	}

	public void setAttrvalue(String attrvalue) {
		this.attrvalue = attrvalue;
	}

}