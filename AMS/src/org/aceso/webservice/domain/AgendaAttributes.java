package org.aceso.webservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AgendaAttributes entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "agenda_attributes")
public class AgendaAttributes implements java.io.Serializable {

	// Fields

	private Integer id;
	private Agenda agenda;
	private String attrname;
	private String attrvalue;

	// Constructors

	/** default constructor */
	public AgendaAttributes() {
	}

	/** full constructor */
	public AgendaAttributes(Agenda agenda, String attrname, String attrvalue) {
		this.agenda = agenda;
		this.attrname = attrname;
		this.attrvalue = attrvalue;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agenda_id", nullable = false)
	public Agenda getAgenda() {
		return this.agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}

	@Column(name = "attrname", nullable = false, length = 20)
	public String getAttrname() {
		return this.attrname;
	}

	public void setAttrname(String attrname) {
		this.attrname = attrname;
	}

	@Column(name = "attrvalue", nullable = false, length = 65535)
	public String getAttrvalue() {
		return this.attrvalue;
	}

	public void setAttrvalue(String attrvalue) {
		this.attrvalue = attrvalue;
	}

}