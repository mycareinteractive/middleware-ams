package org.aceso.webservice.domain;
// default package

import java.util.HashSet;
import java.util.Set;

/**
 * AbstractConfigurations entity provides the base persistence definition of the
 * Configurations entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractConfigurations implements java.io.Serializable {

	// Fields

	private Integer id;
	private String shortdesc;
	private String longdesc;
	private Set roombedConfigs = new HashSet(0);

	// Constructors

	/** default constructor */
	public AbstractConfigurations() {
	}

	/** minimal constructor */
	public AbstractConfigurations(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public AbstractConfigurations(Integer id, String shortdesc,
			String longdesc, Set roombedConfigs) {
		this.id = id;
		this.shortdesc = shortdesc;
		this.longdesc = longdesc;
		this.roombedConfigs = roombedConfigs;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getShortdesc() {
		return this.shortdesc;
	}

	public void setShortdesc(String shortdesc) {
		this.shortdesc = shortdesc;
	}

	public String getLongdesc() {
		return this.longdesc;
	}

	public void setLongdesc(String longdesc) {
		this.longdesc = longdesc;
	}

	public Set getRoombedConfigs() {
		return this.roombedConfigs;
	}

	public void setRoombedConfigs(Set roombedConfigs) {
		this.roombedConfigs = roombedConfigs;
	}

}