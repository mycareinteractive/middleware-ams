package org.aceso.webservice.domain;

public class PatronMenu {
	private String nutri = null;
	private String lock=null;
	private String mealid = null;
	private String mealdate = null;
	private String mrn = null;
	private String url = null;
	/**
	 * @return the nutri
	 */
	public String getNutri() {
		return nutri;
	}
	/**
	 * @param nutri the nutri to set
	 */
	public void setNutri(String nutri) {
		this.nutri = nutri;
	}
	/**
	 * @return the lock
	 */
	public String getLock() {
		return lock;
	}
	/**
	 * @param lock the lock to set
	 */
	public void setLock(String lock) {
		this.lock = lock;
	}
	/**
	 * @return the mealid
	 */
	public String getMealid() {
		return mealid;
	}
	/**
	 * @param mealid the mealid to set
	 */
	public void setMealid(String mealid) {
		this.mealid = mealid;
	}
	/**
	 * @return the mealdate
	 */
	public String getMealdate() {
		return mealdate;
	}
	/**
	 * @param mealdate the mealdate to set
	 */
	public void setMealdate(String mealdate) {
		this.mealdate = mealdate;
	}
	/**
	 * @return the mrn
	 */
	public String getMrn() {
		return mrn;
	}
	/**
	 * @param mrn the mrn to set
	 */
	public void setMrn(String mrn) {
		this.mrn = mrn;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
