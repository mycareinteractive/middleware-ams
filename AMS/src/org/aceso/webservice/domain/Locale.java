package org.aceso.webservice.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Locale entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "locale")
public class Locale implements java.io.Serializable {

	// Fields

	private Integer id;
	private String desc;
	private Set<MenuattributesTranslation> menuattributesTranslations = new HashSet<MenuattributesTranslation>(
			0);
	private Set<MenuTranslation> menuTranslations = new HashSet<MenuTranslation>(
			0);

	// Constructors

	/** default constructor */
	public Locale() {
	}

	/** minimal constructor */
	public Locale(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public Locale(Integer id, String desc,
			Set<MenuattributesTranslation> menuattributesTranslations,
			Set<MenuTranslation> menuTranslations) {
		this.id = id;
		this.desc = desc;
		this.menuattributesTranslations = menuattributesTranslations;
		this.menuTranslations = menuTranslations;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "desc")
	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "locale")
	public Set<MenuattributesTranslation> getMenuattributesTranslations() {
		return this.menuattributesTranslations;
	}

	public void setMenuattributesTranslations(
			Set<MenuattributesTranslation> menuattributesTranslations) {
		this.menuattributesTranslations = menuattributesTranslations;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "locale")
	public Set<MenuTranslation> getMenuTranslations() {
		return this.menuTranslations;
	}

	public void setMenuTranslations(Set<MenuTranslation> menuTranslations) {
		this.menuTranslations = menuTranslations;
	}

}