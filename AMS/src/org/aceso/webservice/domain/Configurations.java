package org.aceso.webservice.domain;
// default package

import java.util.Set;

/**
 * Configurations entity. @author MyEclipse Persistence Tools
 */
public class Configurations extends AbstractConfigurations implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Configurations() {
	}

	/** minimal constructor */
	public Configurations(Integer id) {
		super(id);
	}

	/** full constructor */
	public Configurations(Integer id, String shortdesc, String longdesc,
			Set roombedConfigs) {
		super(id, shortdesc, longdesc, roombedConfigs);
	}

}
