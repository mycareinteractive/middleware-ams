package org.aceso.webservice.domain;
// default package

/**
 * RoombedConfig entity. @author MyEclipse Persistence Tools
 */
public class RoombedConfig extends AbstractRoombedConfig implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public RoombedConfig() {
	}

	/** minimal constructor */
	public RoombedConfig(String room) {
		super(room);
	}

	/** full constructor */
	public RoombedConfig(Configurations configurations, String room, String bed) {
		super(configurations, room, bed);
	}

}
