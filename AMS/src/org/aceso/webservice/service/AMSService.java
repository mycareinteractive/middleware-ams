package org.aceso.webservice.service;

import org.aceso.webservice.jaxb.ClinicalData;
import org.aceso.webservice.jaxb.AgendaResponse;
import org.aceso.webservice.jaxb.ClinicalInfo;
import org.aceso.webservice.jaxb.ConfigSelections;

public interface AMSService {	
	public String generateXML(String portalType, int localeId) throws Exception;	
	
	public String getPatientMrn(String name, String room, String bed) throws Exception;
	public void addPatient() throws Exception;	
	public int findPatientExists(String name, String room, String bed) throws Exception;
	
	public void addConfigSelections(ConfigSelections configSelections) throws Exception;
	public ConfigSelections getConfigSelections( String room, String bed) throws Exception;
	public AgendaResponse findAgendaResponse(int count, String portalType, String dateOverride);

	public ClinicalInfo getClinicalData(String type, String mrn, String numrec,
			String sortorder) throws Exception;
	public void addClinicalData(ClinicalData clinicalData) throws Exception;
}
