package org.aceso.webservice.service;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndDocument;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.aceso.webservice.jaxb.ClinicalData;
import org.aceso.webservice.domain.Agenda;
import org.aceso.webservice.domain.Menu;
import org.aceso.webservice.domain.MenuAttributes;
import org.aceso.webservice.domain.MenuTranslation;
import org.aceso.webservice.domain.MenuattributesTranslation;
import org.aceso.webservice.domain.MirthClinicaldata;
import org.aceso.webservice.domain.RoombedConfig;
import org.aceso.webservice.domain.Slider;
import org.aceso.webservice.jaxb.AgendaItems;
import org.aceso.webservice.jaxb.AgendaResponse;
import org.aceso.webservice.jaxb.ClinicalInfo;
import org.aceso.webservice.jaxb.ClinicalItem;
import org.aceso.webservice.jaxb.ConfigSelections;
import org.aceso.webservice.jaxb.Configuration;
import org.aceso.webservice.utility.DateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service("amsServiceImpl")
public class AMSServiceImpl implements AMSService{

	protected static Logger log = LoggerFactory.getLogger("AMSServiceImpl");
	
	
	public List<MenuAttributes> attributes = new ArrayList<MenuAttributes>();
	public List<Menu> menu = new ArrayList<Menu>();
	public List<Slider> slider = new ArrayList<Slider>();
	private AMSDao amsDao; 
	
	public AMSServiceImpl() {
		log.debug("AMSServiceImpl Constructor executed succesfully");
	}
	
	private Menu getMenuBar(String portal){		
		return amsDao.getMenuBar(portal);
	}
	
	
	private List<MenuAttributes> getAttributesForMenuIdByType(int menuId, String portal, int localeId){
		 List menuAttributesList = this.amsDao.getAttributesForMenuIdBytype(menuId, portal, localeId);
	        if (menuAttributesList != null && menuAttributesList.size() > 0) {
	            log.info("menuAttributesList >0");
	        }
	        if (localeId != 0) {
	            log.info("before setting label");
	            this.setMenuAttributeLocaleLabel(menuAttributesList, localeId);
	            log.info("after setting label");
	        }
	        return menuAttributesList;
	}
	
	/*public List<Menu> getParentMenuItems(int menuBarId, String portal){	
		return amsDao.getParentMenuItems(menuBarId, portal);
	}*/
	
	public List<Menu> getParentMenuItems(int menuBarId, String portal,int localeId){	
		List<Menu> menuList =  amsDao.getParentMenuItems(menuBarId, portal,localeId);
		if(menuList != null && menuList.size() > 0){
			log.info("MenuList >0");
		}
		if(localeId !=0){	
			setLocaleLabel(menuList);
		}		
		return menuList;
	}
	

	private void setLocaleLabel(List<Menu> menuList) {
		for(Menu menuItem: menuList){
			if(!menuItem.getMenuTranslations().isEmpty()){
				Set<MenuTranslation> menuTraslationSet = menuItem.getMenuTranslations();
				if(menuTraslationSet != null && menuTraslationSet.size() > 0){
					log.info("menuTraslationSet >0");
				}
				for(MenuTranslation menuTranslation:menuTraslationSet){
					log.info("for loop");
					if(menuTranslation.getLabel() != null && menuTranslation.getLabel().length()>0){
						log.info("menuTranslation.getLabel() = "+menuTranslation.getLabel());
						menuItem.setLabel(menuTranslation.getLabel());
					} 
				}
			}
		}
	}
	
	private void setMenuAttributeLocaleLabel(List<MenuAttributes> menuAttrsList, int localeId) {
        for (MenuAttributes menuAttrItem : menuAttrsList) {
            if (menuAttrItem.getMenuattributesTranslations().isEmpty()) continue;
            Set<MenuattributesTranslation> menuAttrTraslationSet = menuAttrItem.getMenuattributesTranslations();
            for (MenuattributesTranslation menuattributesTranslation : menuAttrTraslationSet) {
                if (menuattributesTranslation.getAttrvalue() != null && menuattributesTranslation.getAttrvalue().length() > 0) {
                    menuAttrItem.setValue(menuattributesTranslation.getAttrvalue());
                    continue;
                }
                menuAttrItem.setValue("");
            }
        }
    }
	
	public List<Menu> getSubMenuItemsByParentMenuId(int menuId, String portal, int localeId){
		//return amsDao.getSubMenuItemsByParentMenuId(menuId,portal);
		List<Menu> menuList =  amsDao.getSubMenuItemsByParentMenuId(menuId,portal,localeId);
		if(localeId !=0){
			setLocaleLabel(menuList);
		}
		return menuList;
	}	
	
	public AgendaResponse findAgendaResponse(int count, String portalType, String dateOverride) {
		
		AgendaResponse agendaResponse = new AgendaResponse();
		AgendaItems items = null;
		List<AgendaItems> agendaItemsList = new ArrayList<AgendaItems>();
		for(int i=0;i<count;i++){
			items = new AgendaItems();	
			items.setAgendaDate(DateHandler.getDateIncrementByNoOfDays(dateOverride,i));			
			Agenda agenda = amsDao.findAgenda(i,portalType,dateOverride);
			if(agenda != null){
				items.setAgendaEntry(agenda);				
			}else{
				items.setAgendaEntry(null);
			}
			agendaItemsList.add(items);
		}
		agendaResponse.setAgendaList(agendaItemsList);
		return agendaResponse;
	}
	
		//http://www.cowtowncoder.com/blog/archives/cat_xmlstax.html
	//http://sanjaal.com/java/604/java-and-xml/generating-xml-file-using-java-streaming-api-for-xml-stax-a-clean-tutorial/
	//http://www.vogella.de/articles/JavaXML/article.html
    public String generateXML(String portal, int localeId) throws Exception { 
    	log.info("Entering method generateXML for portal = "+portal);
        /** 
         * Create a new instance of the XMLEventFactory object 
         */
        XMLEventFactory xmlEventFactory = XMLEventFactory.newInstance(); 
        /** 
         * Create a new instance of the XMLOutputFactory object 
         */
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance(); 
        XMLEvent newLine = xmlEventFactory.createDTD("\n");
       
        /** 
         * Create a new XMLEventWriter object using the XMLOuputFactory Object 
         * The parameter denotes that the output will be done in XML. 
         */
        StringWriter stringWriter = new StringWriter();

        XMLEventWriter writer = outputFactory.createXMLEventWriter(stringWriter); 
  
        /** 
         * Creates a new instance of a StartDocument event 
         * First Parameters: Document Encoding 
         * Second Parameter: version 
         */
        StartDocument startDocument = xmlEventFactory.createStartDocument( 
                "UTF-8", "1.0"); 
  
        /**Add this document to the writer**/
        writer.add(startDocument); 
        writer.add(newLine);
        /** 
         * Create a new StartElement object and add it to the event writer. 
         * The StartElement interface provides access to information about start elements. 
         * A StartElement is reported for each Start Tag in the document. 
         * 
         * This defaults the NamespaceContext to an empty NamespaceContext. Querying this event for 
         * its namespaces or attributes will result in an empty iterator being returned. 
         * 
         * Parameter 1: Prefix - the prefix of the QName of the new StartElement 
         * Parameter 2: Namespace URI - the uri of the QName of the new StartElement 
         * Parameter 3: Local Name - the local name of the QName of the new StartElement 
         */
        StartElement startElement = xmlEventFactory.createStartElement("", "", 
                "response"); 
        writer.add(startElement); 
        writer.add(newLine);
       
        StartElement menuBarStartElement = xmlEventFactory.createStartElement( 
                "", "", "menuBar"); 
        writer.add(menuBarStartElement); 
        writer.add(newLine);
        
        /** 
         * Under each menuBar element, we will create menu items and any attributes it has 
         */
  
        // We shall remove the parentId=id trick.
        // All menu structure start from the root - menuBar, who has parentId = 0.
        // (ideally should be either null or -1, but don't want to change DB schema)
        // All parent menus (level 1 menus) have parentId pointing to their menuBar.
        // This allows us to build a tree-structure tool for database input.
        Menu menuBar = this.getMenuBar(portal);
        
        List<Menu> menuList = this.getParentMenuItems(menuBar.getId(),portal,localeId);
        
        
       for(Menu menu:menuList){
        	StartElement menuStartElement = xmlEventFactory.createStartElement( 
                    "", "", "menu"); 
        	writer.add(menuStartElement); 
        	writer.add(newLine);        	
        	/*createNode(writer, "tag", menu.getTag());
        	createNode(writer, "label", menu.getLabel());
        	createNode(writer, "image", menu.getImage()); 
        	createNode(writer, "type", menu.getType());*/
        	
        	if(menu.getTag() != null && menu.getTag().length()>0){
    			createNode(writer, "tag", menu.getTag()); 
    			}
//    		        		writer.add(tab);
    			if(menu.getLabel() != null && menu.getLabel().length()>0){
    			createNode(writer, "label", menu.getLabel());
    			}
//    		        		writer.add(tab);
    			if(menu.getImage() != null && menu.getImage().length()>0){
    			createNode(writer, "image", menu.getImage());
    			}
    			
    		 	if(menu.getType() != null && menu.getType().length()>0){
    			createNode(writer, "type", menu.getType());
    			}
    			
    			if(menu.isCaptureClicks()){
    				createNode(writer, "captureclicks","true");
    				}
    			
    			
    			
        	createAttributes(portal, xmlEventFactory, newLine, writer, menu,localeId);
        	if(menu.getType()!=null && menu.getType().equals("menu")){
		        	createChildren(portal, xmlEventFactory, newLine, writer,menu,localeId);
        	}
       	EndElement menuEndElement = xmlEventFactory.createEndElement("", "","menu");         
        	writer.add(menuEndElement); 
        	writer.add(newLine);
        }
        
        if(menuBar.isAttributesInd()){
        	//List<MenuAttributes> attributeList = this.getAttributesForMenuIdByType(menuBar.getId(),"ATR",portal);
        	//for(MenuAttributes attribute: attributeList){
        	//createNode(writer, attribute.getName(), attribute.getValue());
        	//}
        }      
        
        EndElement menuBarEndElement = xmlEventFactory.createEndElement("", "","menuBar");         
    	writer.add(menuBarEndElement); 
    	writer.add(newLine);
       /* if(room!=null && !room.isEmpty()) {
        	if( bed != null && !bed.isEmpty()){
        	logger.debug("Retrieving ConfigSelections for room = "+room+" and bed ="+bed);
	    	JAXBContext jc;
			ConfigSelections configSelections = null;
			try {
				configSelections = getConfigSelections(room, bed);
				jc = JAXBContext.newInstance(ConfigSelections.class);
				Marshaller marshaller = jc.createMarshaller();
				marshaller.setProperty("jaxb.fragment", Boolean.TRUE);
		        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);	        
		        marshaller.marshal(configSelections, writer);	       
			} catch (JAXBException e) {
				logger.error("JAXBException while generatingXML for portal ="+portal,e);
			} catch (Exception e) {
				logger.error("Exception while generatingXML for portal ="+portal,e);
			} 
          }
        }*/
		
        /**Ending other Tags**/
        EndDocument endDocument = xmlEventFactory.createEndDocument(); 
        writer.add(endDocument); 
  
        /** 
         * Flush the content to console (as we used System.out in the EventWriter parameter. 
         * If you use FileStream, it will be flushed to file. 
         */
       
        
        writer.flush();
        
        
       // stringWriter.getBuffer().append(configStringWriter.getBuffer());
        //https://svn.duraspace.org/view/fcrepo-mirror/fedora/trunk/src/java/fedora/server/journal/readerwriter/multicast/JournalEntrySizeEstimator.java?r1=6733&r2=6740&pathrev=6821&sortby=date&view=patch
        String xml = stringWriter.getBuffer().toString();

		
        /** 
         * Remember to close the XMLEventWriter 
         */
        writer.close(); 
        log.info("Exiting method generateXML for portal ="+portal);
        if(log.isDebugEnabled()){
        	log.debug("Returning XML ="+xml);
        }
        
        return xml;
        
    }

	private void createAttributes(String portal,
			XMLEventFactory xmlEventFactory, XMLEvent newLine,
			XMLEventWriter writer, Menu menu,int localeId) throws XMLStreamException {
		
		if(menu.isAttributesInd()){
		List<MenuAttributes> attributeList = this.getAttributesForMenuIdByType(menu.getId(),portal,localeId);
		
			if(attributeList != null && attributeList.size() >0){
			StartElement attributesStartElement = xmlEventFactory.createStartElement( 
			        "", "", "attributes"); 
			writer.add(attributesStartElement); 
			writer.add(newLine);         	
			
			for(MenuAttributes attribute: attributeList){
				
				StartElement attributeStartElement = xmlEventFactory.createStartElement( 
				        "", "", "attribute");
				writer.add(attributeStartElement); 
				 if(attribute.getName() != null && attribute.getValue() != null ){
					 createNode(writer, attribute.getName(), attribute.getValue());
				 }
			  if(attribute.getViewClass() != null && attribute.getViewClass().length() >0){
				createNode(writer, "class", attribute.getViewClass());
			  }
			  EndElement attributeEndElement = xmlEventFactory.createEndElement("", "","attribute");      
			  writer.add(attributeEndElement);
			}
			
			EndElement attributesEndElement = xmlEventFactory.createEndElement("", "","attributes");         
			writer.add(attributesEndElement); 
			writer.add(newLine);
		}
		}
	}

	private void createChildren(String portal, XMLEventFactory xmlEventFactory,
			XMLEvent newLine, XMLEventWriter writer, Menu menu,int localeId)
			throws XMLStreamException {
		List<Menu> submenuList = this.getSubMenuItemsByParentMenuId(menu.getId(),portal,localeId);        	 
		for(Menu submenu:submenuList){
//		        		writer.add(tab);
			StartElement submenuStartElement = xmlEventFactory.createStartElement( 
		        "", "", menu.getTag()); 
			writer.add(submenuStartElement); 
			writer.add(newLine);
//		        		writer.add(tab);
			if(submenu.getTag() != null && submenu.getTag().length()>0){
			createNode(writer, "tag", submenu.getTag()); 
			}
//		        		writer.add(tab);
			if(submenu.getLabel() != null && submenu.getLabel().length()>0){
			createNode(writer, "label", submenu.getLabel());
			}
//		        		writer.add(tab);
			if(submenu.getImage() != null && submenu.getImage().length()>0){
			createNode(writer, "image", submenu.getImage());
			}
			
			if(submenu.getType() != null && submenu.getType().length()>0){
			createNode(writer, "type", submenu.getType());
			}
			
			if(submenu.isCaptureClicks()){
				createNode(writer, "captureclicks","true");
				}
			
			
			createAttributes(portal, xmlEventFactory, newLine, writer, submenu,localeId);
			
			if(submenu.getType() != null && submenu.getType().equals("menu")) {
				createChildren(portal, xmlEventFactory, newLine, writer,submenu,localeId);
			}

			
			EndElement studentEndElement = xmlEventFactory.createEndElement("", "",menu.getTag());         
			writer.add(studentEndElement); 
			writer.add(newLine);
      }
	}

	
    
    private static void createNode(XMLEventWriter eventWriter, String name,
			String value) throws XMLStreamException {

		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		//XMLEvent end = eventFactory.createDTD("\n");
		//XMLEvent tab = eventFactory.createDTD("\t");
		// Create Start node
		StartElement sElement = eventFactory.createStartElement("", "", name);
		//eventWriter.add(tab);
		eventWriter.add(sElement);
		// Create Content
		 
		Characters characters = eventFactory.createCharacters(value);
		eventWriter.add(characters);
		// Create End node
		EndElement eElement = eventFactory.createEndElement("", "", name);
		eventWriter.add(eElement);
		//eventWriter.add(end);

	}

	/**
	 * @return the amsDAO
	 */
	public AMSDao getAmsDao() {
		return amsDao;
	}

	/**
	 * @param amsDAO the amsDAO to set
	 */
	public void setAmsDao(AMSDao amsDao) {
		this.amsDao = amsDao;
	}

	@Override
	public String getPatientMrn(String name, String room, String bed)
			throws Exception {
		bed = room + bed;
		//TODO bed uppercase?
		return amsDao.getPatientMrn(name, room, bed);
		//return "MRN12345";
	}

	@Override
	public void addPatient() throws Exception {
		amsDao.addPatient();
	}

	@Override
	public int findPatientExists(String name, String room, String bed)
			throws Exception {
		bed = room + bed;
		//TODO bed uppercase?
		return amsDao.findPatientExists(name, room, bed);
	}

	@Override
	public void addConfigSelections(ConfigSelections configSelections)
			throws Exception {
		amsDao.addConfigSelections(configSelections);
	}

	@Override
	public ConfigSelections getConfigSelections(String room, String bed) throws Exception {
		List<RoombedConfig> list = amsDao.getConfigSelections(room, bed);
		ConfigSelections configSelections = null;
		Configuration config = null;
		List<Configuration> configList = new ArrayList<Configuration>();
		boolean firstEntry = false;
		if(list != null){
			for(RoombedConfig roombedConfig:list){
				if(roombedConfig != null && !firstEntry){
					configSelections = new ConfigSelections(roombedConfig.getRoom(), roombedConfig.getBed());
					firstEntry = true;
				}
				if(roombedConfig.getConfigurations() != null){
					config = new Configuration(roombedConfig.getConfigurations().getId().intValue());
					configList.add(config);
				}				
			}
			if(configSelections != null && configList.size() > 0){
				configSelections.setConfigList(configList);
			}
		}
		return configSelections;
	}

	public ClinicalInfo getClinicalData(String type, String mrn, String numrec,
			String sortorder) throws Exception {
			ClinicalInfo clinicalInfo = new ClinicalInfo();
			List<MirthClinicaldata> list = amsDao.getClinicalData(type, mrn,numrec,sortorder);
			List<ClinicalItem> clinicalItems = new ArrayList<ClinicalItem>();
			boolean firstEntry = false;
			
			if(list != null){
				for(MirthClinicaldata data:list){
					if(data != null && !firstEntry){
						clinicalInfo = new ClinicalInfo(data.getType(), data.getMrn());
						firstEntry = true;
					}	
					if(list != null && list.size() > 0){
						String startTime = (data.getStartDateTime() != null) ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getStartDateTime()): null;
						String endTime = (data.getEndDateTime() != null) ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getEndDateTime()): null;
						String recvTime = (data.getRecvDateTime() != null) ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data.getRecvDateTime()): null;
						clinicalItems.add(new ClinicalItem(data.getId(),data.getPtname(), data.getRoom(), data.getBed(), data.getCode(),
								data.getCodeDescription(), data.getValue(), startTime,
								endTime, recvTime,data.getOrderStatus(),data.getOrderNumber()));
					}
				}
				clinicalInfo.setClinicalItems(clinicalItems);
			}
			
			return clinicalInfo;
		}	
	
	public void addClinicalData(ClinicalData clinicalData) throws Exception {
		amsDao.addClinicalData(clinicalData);
	}
}
