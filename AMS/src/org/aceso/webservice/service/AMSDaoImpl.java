package org.aceso.webservice.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.aceso.webservice.jaxb.ClinicalData;
import org.aceso.webservice.domain.*;
import org.aceso.webservice.jaxb.ConfigSelections;
import org.aceso.webservice.jaxb.Configuration;
import org.aceso.webservice.utility.DateHandler;
import org.springframework.util.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.RowMapper; 
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource; 
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


public  class AMSDaoImpl extends HibernateDaoSupport implements AMSDao{
	private static final Logger log = LoggerFactory.getLogger("AMSDaoImpl");
	public List<MenuAttributes> attributes = null;
	public List<Menu> menuList = null;
	public List<Slider> slider = null; 
	
	protected NamedParameterJdbcTemplate namedParameterJdbcTemplate = null;
	
	public Agenda findAgenda(int seqNbr, String portalType,String dateOverride) { 

		final String sql = "SELECT a.id, a.name, a.content, a.contentType, a.portalType, a.languageId "+
							"FROM agenda AS a "+
							"INNER JOIN agenda_attributes t1 ON a.id = t1.agenda_id and t1.attrname = 'start_date' "+
							"INNER JOIN agenda_attributes t2 ON a.id = t2.agenda_id and t2.attrname = 'repeat_days' "+
							"WHERE DATEDIFF(DATE_ADD(:currDate, INTERVAL :seqNbr DAY),t1.attrvalue) % CONVERT(t2.attrvalue, SIGNED INTEGER) = 0 "+
							"AND a.portalType = :portalType";

		MapSqlParameterSource sqlParams = 
		new MapSqlParameterSource(); 
		sqlParams.addValue("seqNbr", seqNbr);
		sqlParams.addValue("portalType", portalType);
		sqlParams.addValue("currDate", DateHandler.getCurrentOrOverrideDateAsString(dateOverride));

		List<Agenda> agendaList = (List<Agenda>) this.namedParameterJdbcTemplate.query(sql, sqlParams, new RowMapper<Agenda>() {
		 public Agenda mapRow(ResultSet rs, int rowNum) throws SQLException {
			 Agenda agenda = new Agenda(); 
			 agenda.setId(rs.getInt("id")); 
			 //String content = rs.getString("content");
			 //agenda.setContent(StringUtils.replace(content,"\"", "&#34;"));
			 agenda.setContent(rs.getString("content"));
			 agenda.setContentType(rs.getString("contenttype"));
			 agenda.setName(rs.getString("name"));
			 agenda.setLanguageId(rs.getInt("languageId"));
			 agenda.setPortalType(rs.getString("portalType"));
			return agenda; 
		 }
		 
		}); 
		if(agendaList.size() > 0)
			return agendaList.get(0);
		else
			return null;
		}
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate){
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	@Override
	public List<MenuAttributes> getAttributesForMenuIdBytype(int menuId, String portal, int localeId) {
		if(localeId == 0){
		Object[] queryParam = {menuId,portal};
		return getHibernateTemplate().find("from MenuAttributes where menuId = ? and portal = ?",queryParam);
		}else{
			Object[] queryParam = {menuId, portal, localeId};
			return getHibernateTemplate().find("from MenuAttributes m left join fetch m.menuattributesTranslations mt where m.menuId = ? and m.portal = ? and mt.locale.id =?",queryParam);
		}
	}
	
	
	public Menu getMenuBar(String portal) {		
		@SuppressWarnings("rawtypes")
		List list = getHibernateTemplate().find("from Menu where parentId = 0 and portal = ?",portal);
		return (Menu)list.get(0); 
	}

	public Menu getMenuByMenuId(int menuId,String portal) {
		Object[] queryParam = {menuId, portal};
		@SuppressWarnings("rawtypes")
		List list = getHibernateTemplate().find("from Menu where id=? and portal = ?",queryParam);
		return (Menu)list.get(0);
	}

	/*@SuppressWarnings("unchecked")
	public List<Menu> getParentMenuItems(int menuBarId,String portal) {
		Object[] queryParam = {menuBarId, portal};
		return getHibernateTemplate().find("from Menu where parentId = ? and Id != 0 and portal = ?",queryParam);
	}*/
	@SuppressWarnings("unchecked")
	public List<Menu> getParentMenuItems(int menuBarId,String portal, int localeId) {
		if(localeId == 0){
			Object[] queryParam = {menuBarId, portal};
			return getHibernateTemplate().find("from Menu where parentId = ? and id != 0 and portal = ?",queryParam);
		}else{
			log.info("Else of getParentMenuItems");
			Object[] queryParam = {menuBarId, portal, localeId};
			return getHibernateTemplate().find("from Menu m left join fetch m.menuTranslations mt where m.parentId = ? and m.id != 0 and m.portal = ? and mt.locale.id =?",queryParam);
		}
	}
	@SuppressWarnings("unchecked")
	public List<Slider> getSliderByMenuId(int menuId,String portal) {
		Object[] queryParam = {menuId, portal};
		return getHibernateTemplate().find("from Slider where menuId = ? and portal = ?",queryParam);
	}

	@SuppressWarnings("unchecked")
	public List<Menu> getSubMenuItemsByParentMenuId(int menuId,String portal, int localeId) {
		if(localeId == 0){
			Object[] queryParam = {menuId, portal};
			return getHibernateTemplate().find("from Menu where parentId = ? and id != parentId and portal = ?",queryParam);
		}else{
			Object[] queryParam = {menuId, portal, localeId};
			return getHibernateTemplate().find("from Menu m left join fetch m.menuTranslations mt where m.parentId = ? and m.id != m.parentId and m.portal = ? and mt.locale.id =?",queryParam);
		}
	}
	
	
@Override
public String getPatientMrn(String name, String room, String bed)
		throws Exception {
	String mrn = null;
	Object[] queryParam  = {name , room, bed};
	@SuppressWarnings("unchecked")
	List<PatronInfo> patron = getHibernateTemplate().find("from PatronInfo where name = ? and room = ? and bed = ?",queryParam);
	if(patron.size() > 0){
		mrn =  patron.get(0).getMrn();
	}
	return mrn;
}

public int findPatientExists(String name, String room, String bed)
		throws Exception {
	Object[] queryParam  = {name , room, bed};
	@SuppressWarnings({ "rawtypes" })
	List list = getHibernateTemplate().find("select count(*) from PatronInfo where name = ? and room = ? and bed = ?",queryParam);
	
    Long count = (Long) list.get(0);
    return count.intValue();
}

@Override
@Transactional
public void addPatient() throws Exception {
	PatronInfo patron = new PatronInfo();
	patron.setId("14");
	patron.setName("JOHNm SMITHm");
	patron.setRoom("3421");
	patron.setBed("3422p1");
	patron.setMrn("MRN123456");
	log.debug("Add patient1111");
	getHibernateTemplate().save(patron);
	log.debug("Added patient");
}


@Override
@Transactional(value="txManager", readOnly=false, propagation=Propagation.REQUIRES_NEW, rollbackFor={Exception.class})
public void addConfigSelections(ConfigSelections configSelections)

        throws Exception {

log.info("Adding ConfigSelections for "+configSelections.getRoom()+":"+configSelections.getBed());

Session session = null;

Transaction txn = null;

try{

  session = getSessionFactory().getCurrentSession();

  txn = session.beginTransaction();

  removeConfigSelections(configSelections, session);

  if(configSelections.getConfigList().size() > 0){

  for(Configuration config:configSelections.getConfigList()){

    RoombedConfig roomBed = new RoombedConfig(new Configurations(config.getId()),configSelections.getRoom(),configSelections.getBed());

    log.info("Saving New Settings ="+config.getId());

    session.save(roomBed);

  }

              

  }

  txn.commit();

}catch(Exception e){

    log.error("Exception occured while adding ConfigurationSelection "+configSelections,e);

    if(session.getTransaction() != null && session.getTransaction().isActive()){

            session.getTransaction().rollback();

    }

}

}


@Override
public List<RoombedConfig> getConfigSelections(String room, String bed) throws Exception {
	Object[] queryParam = {room,bed};
	List list = getHibernateTemplate().find("from RoombedConfig where room = ? and bed = ?",queryParam);
	if(list != null)
		log.debug("List size "+list.size());
	return list;
}

private void removeConfigSelections(ConfigSelections configSelections, Session session)
{
    Query query =session.createQuery("from RoombedConfig rmc where rmc.room = :room and rmc.bed = :bed ");
    query.setString("room", configSelections.getRoom());
    query.setString("bed", configSelections.getBed());
    List<RoombedConfig> deleteRecord = query.list();
    for (RoombedConfig roombedConfig : deleteRecord)
    {
    	log.info("Removing existing selection for Room and Bed = "+roombedConfig.getBed()+":"+roombedConfig.getBed());
    	session.delete(roombedConfig);
    }
}
@Override
public List<MirthClinicaldata> getClinicalData(String type, String mrn,
        String numrec, String sortOrder)  throws Exception {
  Object[] queryParam = {type,mrn};
  String query = null;
  if(sortOrder != null && sortOrder.equals("desc")){
        query = "from MirthClinicaldata where type = ? and mrn = ? order by recvDateTime desc";
  }else{
        query = "from MirthClinicaldata where type = ? and mrn = ? order by recvDateTime asc";
  }
  @SuppressWarnings("unchecked")
  List<MirthClinicaldata> list = getHibernateTemplate().find(query,queryParam);
  if(numrec != null && !numrec.isEmpty()){
  if(list != null && list.size() > Integer.parseInt(numrec)){
        list = list.subList(0, Integer.parseInt(numrec));
  }
}
  return list;
}

@Override
@Transactional(value="txManager", readOnly=false, propagation=Propagation.REQUIRES_NEW, rollbackFor={Exception.class})
public void addClinicalData(ClinicalData clinicalData)
		throws Exception {
    log.info("Adding addClinicalData for "+clinicalData.getRoom()+":"+clinicalData.getBed()+":"+clinicalData.getMrn());
    Session session = null;
    Transaction txn = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try{
    	session = getSessionFactory().getCurrentSession();
    	txn = session.beginTransaction();
    	Object[] queryParam = {clinicalData.getId()};
    	String query = "from MirthClinicaldata where id = ?";
    	List<MirthClinicaldata> list =  getHibernateTemplate().find(query,queryParam);
    	MirthClinicaldata data = null;
    	 if(list != null && list.size()>0){
    		 data = list.get(0);
    	  }else{
    		  data = new MirthClinicaldata();
    	  }
    	 
   	 
    	if(clinicalData.getPtname() != null && clinicalData.getPtname().length() > 0)
    		data.setPtname(clinicalData.getPtname());
    	
    	if(clinicalData.getRoom() != null && clinicalData.getRoom().length() > 0)
    		data.setRoom(clinicalData.getRoom());
    	
    	if(clinicalData.getBed() != null && clinicalData.getBed().length() > 0)
    		data.setBed(clinicalData.getBed());
    	
    	if(clinicalData.getMrn() != null && clinicalData.getMrn().length() > 0)
    		data.setMrn(clinicalData.getMrn());
    	
    	if(clinicalData.getType() != null && clinicalData.getType().length() > 0)
    		data.setType(clinicalData.getType());
    	
    	if(clinicalData.getCode() != null && clinicalData.getCode().length() > 0)
    		data.setCode(clinicalData.getCode());
    	
    	if(clinicalData.getCodeDescription() != null && clinicalData.getCodeDescription().length() > 0)
    		data.setCodeDescription(clinicalData.getCodeDescription());
    	
    	if(clinicalData.getValue() != null && clinicalData.getValue().length() > 0)
    		data.setValue(clinicalData.getValue());
    	
    	if(clinicalData.getStartDateTime() != null && clinicalData.getStartDateTime().length() > 0)
    		data.setStartDateTime(sdf.parse(clinicalData.getStartDateTime()));
    	
    	if(clinicalData.getEndDateTime() != null && clinicalData.getEndDateTime().length() > 0)
    		data.setEndDateTime(sdf.parse(clinicalData.getEndDateTime()));
    	
    	data.setRecvDateTime(sdf.parse(sdf.format(new Date())));
    	
    	session.saveOrUpdate(data);	
    	txn.commit();
    	
    }catch(Exception e){
        log.error("Exception occured while adding ConfigurationSelection "+clinicalData,e);
        if(session.getTransaction() != null && session.getTransaction().isActive()){
                session.getTransaction().rollback();
        }
    }
}

}
