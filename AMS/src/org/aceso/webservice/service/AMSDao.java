package org.aceso.webservice.service;

import java.util.List;

import org.aceso.webservice.domain.Agenda;
import org.aceso.webservice.domain.Menu;
import org.aceso.webservice.domain.MenuAttributes;
import org.aceso.webservice.domain.MirthClinicaldata;
import org.aceso.webservice.domain.RoombedConfig;
import org.aceso.webservice.domain.Slider;
import org.aceso.webservice.jaxb.ClinicalData;
import org.aceso.webservice.jaxb.ConfigSelections;

public interface AMSDao {
	
	public Menu getMenuBar(String portal);
	public Menu getMenuByMenuId(int menuId, String portal);
	public List<MenuAttributes> getAttributesForMenuIdBytype(int menuId, String portal, int localeId);
	//public List<Menu> getParentMenuItems(int menuBarId,String portal);
	public List<Menu> getParentMenuItems(int menuBarId,String portal, int localeId);	
	public List<Menu> getSubMenuItemsByParentMenuId(int menuId,String portal, int localeId);
	
		
	public String getPatientMrn(String name, String room, String bed) throws Exception;
	public void addPatient() throws Exception;
	public int findPatientExists(String name, String room, String bed) throws Exception;
	
	public void addConfigSelections(ConfigSelections configSelections) throws Exception;
	public List<RoombedConfig> getConfigSelections( String room, String bed) throws Exception;

	public Agenda findAgenda(int seqNbr, String portalType, String dateOverride);
	public List<MirthClinicaldata> getClinicalData(String type, String mrn,
			String numrec, String sortorder) throws Exception;
	
	public void addClinicalData(ClinicalData clinicalData) throws Exception;
}
